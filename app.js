Vue.component("tree-data-table", {
  props: ["headers", "items", "item-key"],
  data: function () {
    return {
      expanded: [],
    };
  },
  methods: {
    findChildren: function (currentNode) {
      let children = [];

      (function allDescendants(currentNode) {
        if (!currentNode._children) {
          return;
        }
        for (const child of currentNode._children) {
          children.push(child);
          allDescendants(child);
        }
      })(currentNode);
      return children;
    },
    expandItem: function (item) {
      let vm = this;
      let ix = vm.expanded.indexOf(item);
      let children = vm.findChildren(item);
      if (ix > -1) {
        for (const child of children) {
          let childIndex = vm.expanded.indexOf(child);
          if (childIndex > -1) {
            vm.expanded.splice(childIndex, 1);
          }
        }
        vm.expanded.splice(ix, 1);
      } else {
        vm.expanded.push(item);
      }
    },
    getExpandIcon(item) {
      return this.expanded.indexOf(item) > -1
        ? "mdi-chevron-up"
        : "mdi-chevron-down";
    },
    getPadding(level) {
      let padding = (level - 1) * 2;
      return { padding: padding.toString() + "rem" };
    },
    getButtonColor(chidren) {
      let isExpandable = this.isExpandable(chidren);
      color = isExpandable ? "" : "#00000000";
      return { color: color + "!important" };
    },
    isExpandable(children) {
      return children && children.length > 0;
    },
  },
  computed: {
    itemsComputed() {
      let items = [...this.items];
      items.map((a) => (a.level = 1));

      for (var item of this.expanded) {
        let ix = items.indexOf(item);
        let level = item.level;
        let children = item._children;
        children.map((a) => (a.level = level + 1));
        items.splice(ix + 1, 0, ...children);
      }
      return items;
    },
    headersComputed() {
      let headers = [...this.headers];
      let width = 100 / headers.length;
      headers[0].width = `${width}%`;
      return headers;
    },
  },
  template: `<v-card class="ma-5">
                <v-data-table :headers="headersComputed" :items="itemsComputed">
                <template v-slot:body="{ items, headers }">
                    <tbody>
                        <tr v-for="item in items" :key="item.id">
                            <td v-for="(header, ix) in headers"> 
                                <template v-if="ix == 0">
                                    <span :style="getPadding(item.level)">
                                        <v-icon @click="expandItem(item)" :style="getButtonColor(item._children)" :disabled="!isExpandable(item._children)">{{ getExpandIcon(item) }}</v-icon> 
                                        {{ item[header.value] }}
                                    </span>
                                </template>
                                <template v-else>{{ item[header.value] }}</template>
                            </td>
                        </tr>
                    </tbody>
                </template>
                </v-data-table>
             </v-card>`,
});

const app = new Vue({
  el: "#app",
  vuetify: new Vuetify(),
  data: function () {
    return {
      headers: [
        { text: "First name", value: "firstName" },
        { text: "Last name", value: "lastName" },
      ],
      rows: [
        {
          id: 1,
          firstName: "Josephine",
          lastName: "Astrid",
          _children: [
            {
              id: 4,
              firstName: "Filip",
              lastName: "Van Belgie",
            },
          ],
        },
        {
          id: 2,
          firstName: "Boudewijn",
          lastName: "Van Brabandt",
        },
        {
          id: 3,
          firstName: "Albert II",
          lastName: "Van Belgie",
          _children: [
            {
              firstName: "Filip",
              lastName: "Van Belgie",
              _children: [
                {
                  firstName: "Elisabeth",
                  lastName: "Van Brabant",
                },
                {
                  firstName: "Gabriel",
                  lastName: "Boudwijn",
                },
                {
                  id: 5,
                  firstName: "Emmanuel",
                  lastName: "Van Belgie",
                  _children: [
                    {
                      firstName: "Gabriel",
                      lastName: "Boudwijn",
                    },
                  ]
                },
                {
                  firstName: "Eleonore",
                  lastName: "Boudwijn",
                },
              ],
            },
            {
              firstName: "Astrid",
              lastName: "Van Belgie",
            },
            {
              firstName: "Laurent",
              lastName: "Van Belgie",
            },
          ],
        },
      ],
    };
  },
});
